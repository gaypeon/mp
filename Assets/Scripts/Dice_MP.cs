﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Dice_MP : MonoBehaviour
{
    [SerializeField] private Transform[] walls;
    [SerializeField] private new Rigidbody rigidbody;
    [SerializeField] private Text text;
    [SerializeField] private float maxMagnitude;

    private Vector3 startPos;
    private Quaternion startRot;

    public int Outcome
    {
        get
        {
            int _result = 0;
            for (int i = 1; i < walls.Length; i++)
                if (walls[i].GetChild(0).position.y > walls[_result].GetChild(0).position.y) _result = i;
            return _result + 1;
        }
    }

    private void Start()
    {
        startPos = rigidbody.position;
        startRot = rigidbody.rotation;
    }

    public IEnumerator Throw(float forwardForce, float leftForce)
    {
        rigidbody.isKinematic = false;
        rigidbody.AddForce(forwardForce * transform.forward);
        rigidbody.AddForce(leftForce * transform.right);
        rigidbody.AddTorque(forwardForce * transform.right);
        do
        {
            yield return new WaitForSeconds(1);
        }
        while (rigidbody.velocity.magnitude >= maxMagnitude);
    }

    public void Reset()
    {
        text.text = "";
        rigidbody.isKinematic = true;
        rigidbody.velocity = Vector3.zero;
        rigidbody.MovePosition(startPos);
        rigidbody.MoveRotation(startRot);
    }
}