﻿using System.Collections;
using UnityEngine;

public class Illuminating_MP : MonoBehaviour
{
    [SerializeField] private Color[] colors;
    [SerializeField] private float timeOfTransition;
    [SerializeField] private new Renderer renderer;

    private void Start()
    {
        StartCoroutine(Illuminate());
    }

    private IEnumerator Illuminate()
    {
        float _timer = 0;
        int i = 1;
        int _prevI = 0;

        renderer.material.color = colors[0];

        while (true)
        {
            _timer += Time.deltaTime;
            renderer.material.color = Color.Lerp(colors[_prevI], colors[i], _timer / timeOfTransition);

            if (_timer >= timeOfTransition)
            {
                _prevI = i;
                i++;
                i %= colors.Length;
            }

            _timer %= timeOfTransition;

            yield return 0;
        }
    }
}