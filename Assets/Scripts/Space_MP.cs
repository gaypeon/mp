﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Space_MP : MonoBehaviour
{
    public string spaceName;
    public List<Pawn_MP> pawnsHere;

    public Vector3 Pos { get { return transform.position; } }
}