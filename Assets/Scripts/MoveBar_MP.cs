﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MoveBar_MP : MonoBehaviour
{
    [SerializeField] private int axis;
    [SerializeField] private float lowest;
    [SerializeField] private float highest;
    [SerializeField] private float speed;
    [SerializeField] private Color maximumColor;
    [SerializeField] private Color minimumColor;
    [SerializeField] private Image bar;
    [SerializeField] private Image measure;
    [SerializeField] private bool changePivot;

    private int dir = 1;
    private bool doMove;

    public float Value
    {
        get
        {
            return measure.rectTransform.localPosition[axis] / highest;
        }
    }

    public IEnumerator Show()
    {
        Player_MP.isThrowing = true;
        gameObject.SetActive(true);
        yield return StartCoroutine(MoveBar());
        gameObject.SetActive(false);
        Player_MP.isThrowing = false;
    }

    private IEnumerator MoveBar()
    {
        doMove = true;
        float _size = 0;
        while (doMove)
        {
            bar.color = Color.Lerp(minimumColor, maximumColor, bar.rectTransform.rect.width / highest);
            Vector2 _sizeDelta = bar.rectTransform.sizeDelta;
            _sizeDelta[axis] += Time.deltaTime * speed * dir;
            bar.rectTransform.sizeDelta = _sizeDelta;

            _size = axis == 0 ? bar.rectTransform.rect.width : bar.rectTransform.rect.height;

            if (_size <= lowest || _size >= highest)
            {
                if (_size <= lowest && changePivot)
                {
                    Vector2 _newPivot = Vector2.one * 0.5f;
                    _newPivot[axis] = bar.rectTransform.pivot[axis] == 1 ? 0 : 1;
                    bar.rectTransform.pivot = _newPivot;
                }

                dir = dir == 1 ? -1 : 1;
            }

            if (Input.GetKeyDown(KeyCode.D)) doMove = !doMove;

            yield return 0;
        }

        Vector2 _newPos = measure.rectTransform.localPosition;
        _newPos[axis] = _size * (changePivot ? (bar.rectTransform.pivot[axis] == 1 ? -1 : 1) : 1);
        measure.rectTransform.localPosition = _newPos;
    }
}