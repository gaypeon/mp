﻿using UnityEngine;

public class Map_MP : MonoBehaviour
{
    public static Map_MP instance;

    [SerializeField] private Space_MP[] spaces;

    void Awake()
    {
        instance = this;
    }

    public static Space_MP GetSpace(ref int index_)
    {
        index_ %= instance.spaces.Length;
        return instance.spaces[index_];
    }
}