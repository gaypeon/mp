﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn_MP : MonoBehaviour
{
    public delegate void Moving();
    public event Moving DoneMoving;

    public Player_MP owner;
    public int currPos = 0;

    [SerializeField] private float a = -0.01f;
    [SerializeField] private float moveSpeed;

    public IEnumerator MoveSpacesForward(int spaces_)
    {
        int _startPos = currPos;

        for (int i = _startPos + 1; i <= _startPos + spaces_; i++)
        {
            yield return StartCoroutine(Move(i));
        }
    }

    private IEnumerator Move(int destIndex_)
    {
        int _axis = -1;

        for (int i = 0; i < 3; i++)
        {
            if (i == 1) continue;

            if (Map_MP.GetSpace(ref currPos).Pos[i] == Map_MP.GetSpace(ref destIndex_).Pos[i])
            {
                _axis = 2 - i;
                break;
            }
        }

        float _x0 = transform.position[_axis];
        float _x1 = Map_MP.GetSpace(ref destIndex_).Pos[_axis];

        float _startY = transform.position.y;
        int _sign = Map_MP.GetSpace(ref destIndex_).Pos[_axis] - transform.position[_axis] > 0 ? 1 : -1;

        while (Mathf.Abs(transform.position[_axis] - Map_MP.GetSpace(ref destIndex_).Pos[_axis]) > 0.05f)
        {
            float _newPosInAxis = transform.position[_axis] + moveSpeed * _sign * Time.deltaTime;

            Vector3 _newPos = transform.position;
            _newPos[_axis] = _newPosInAxis;
            _newPos.y = _startY + F(_newPosInAxis, a, _x0, _x1);

            transform.position = _newPos;

            yield return 0;
        }

        currPos = destIndex_;
        Vector3 _finalPos = transform.position;
        _finalPos.y = _startY;
        _finalPos[_axis] = Map_MP.GetSpace(ref destIndex_).Pos[_axis];
        transform.position = _finalPos;
    }

    private float F(float x, float a, float x1, float x2)
    {
        return a * (x - x1) * (x - x2);
    }
}