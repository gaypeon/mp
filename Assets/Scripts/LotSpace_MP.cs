﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LotSpace_MP : Space_MP
{
    public enum LOT_CATEGORY { one, two, three, four, five, six, seven, eight }
    public LOT_CATEGORY lotCategory;

    public string lotName;
    public int value;
    public List<Building_MP> buildingsHere;
}