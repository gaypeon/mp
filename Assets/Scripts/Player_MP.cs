﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_MP : MonoBehaviour
{
    public static bool isThrowing;

    public List<LotSpace_MP> myLots;
    public List<Building_MP> myBuildings;

    [SerializeField] private Dice_MP myDice;
    [SerializeField] private Pawn_MP myPawn;
    [SerializeField] private MoveBar_MP moveBarHor;
    [SerializeField] private MoveBar_MP moveBarVer;
    [SerializeField] private float maxForwardForce;
    [SerializeField] private float maxLeftForce;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A) && !isThrowing)
            StartCoroutine(Throw());
    }

    private IEnumerator Throw()
    {
        yield return StartCoroutine(moveBarVer.Show());
        yield return StartCoroutine(moveBarHor.Show());
        yield return StartCoroutine(myDice.Throw(moveBarVer.Value * maxForwardForce, moveBarHor.Value * maxLeftForce));
        yield return StartCoroutine(myPawn.MoveSpacesForward(myDice.Outcome));
        myDice.Reset();
    }
}